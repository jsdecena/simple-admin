## Super Simple Admin 
* using the Laravel PHP Framework*

# How to use
* Clone the repository on your local folder
* Change the database config to match your database settings. ( db name, db user, db pass )
* ::IMPT:: Run "php composer.phar self-update" //If you need to update your composer file.
* ::IMPT:: Run "php composer.phar update" //Needs internet connection to update all the dependencies.
* Run " *php artisan migrate --seed* " on your terminal.
* Enjoy! 

# Details: Email: admin@admin.com Pass: Testing123