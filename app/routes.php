<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('login', 'LoginController@index');
Route::post('login', 'LoginController@login');

/*ADMIN FACING ROUTES*/
Route::group(array('before' => 'auth'), function()
{
	Route::controller('admin', 'AdminController');
	Route::controller('users', 'UsersController');
});