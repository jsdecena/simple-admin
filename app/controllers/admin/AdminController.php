<?php

class AdminController extends AdminControllerCore
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getIndex()
	{
		$this->layout->content = View::make('admin.dashboard');
	}

	public function getRegister()
	{
	   $this->layout->content = View::make('admin.register');
	}

	public function getLogin()
	{
		if (Auth::check()) :
			$this->layout->content = View::make('admin.dashboard');
		else :
			$this->layout->content = View::make('admin.login');
		endif;
	}

	public function getDashboard()
	{		
		if (Auth::check()) :
			$this->layout->content = View::make('admin.dashboard');
		else :
			return Redirect::to('admin/login')->with('error', 'You need to log in to view this page.');
		endif;
	}

	public function getLogout()
	{
		Auth::logout();
		return Redirect::to('admin/login')->with('error', 'Your have been logged out to the application.');
	}	

	public function postRegister() {
		
		$rules = array(
			'email'=>'required|email|unique:users',
			'password'=>'required|alpha_num|between:8,12'
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->passes()) :
			$user = User::create(array(
				'email' 	=> Input::get('email'),
				'password' 	=> Hash::make(Input::get('password'))
			));
			return Redirect::to('admin/login')->with('success', 'Thanks for registering! Please log-in now.');
		else :
			return Redirect::to('admin/register')->with('message', 'The following errors occurred')->withErrors($validator)->withInput();
		endif;
	}	
}