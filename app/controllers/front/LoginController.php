<?php

class LoginController extends FrontControllerCore
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->layout->content = View::make('admin.login');
	}

	public function login()
	{
		$rules = array(
			'email'=>'required|email',
			'password'=>'required'
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->passes()) :

			$userdata = array(
			    'email'      => Input::get('email'),
			    'password'   => Input::get('password') 
			);

			if (Auth::attempt($userdata)) :
				return Redirect::intended('admin/dashboard')->with('message', 'You are now logged in!');			
			endif;

			return Redirect::to('login')->with('error', 'Your email/password combination is incorrect. Please try again.')->withInput();
		else:
			return Redirect::to('login')->withErrors($validator)->withInput(Input::except('password'));
		endif;
	}
}