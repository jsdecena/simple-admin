<?php

class FrontControllerCore extends BaseControllerCore
{
	public function __construct()
	{
		$this->layout = "front.tpl.main";
	}
}